#!/usr/bin/env php
<?php

/*
 * This file is part of jgxvx/cilician-runner.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

use Cache\Adapter\Filesystem\FilesystemCachePool;
use GuzzleHttp\Client;
use Jgxvx\Cilician\Service\Cilician;
use Jgxvx\Cilician\Service\HaveIBeenPwnedApiClient;
use Jgxvx\Cilician\Service\PwnedPasswordsApiClient;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;

if (\file_exists($a = __DIR__ . '/../../autoload.php')) {
    require_once $a;
} else {
    require __DIR__ . '/vendor/autoload.php';
}

$filesystemAdapter = new Local(__DIR__ . '/var/cache/');
$filesystem        = new Filesystem($filesystemAdapter);
$cache             = new FilesystemCachePool($filesystem, 'cilician');
$apiKey            = \getenv('HIBP_API_KEY');

if (!\is_string($apiKey)) {
    \fwrite(STDERR, "HIBP_API_KEY environment variable is not set." . \PHP_EOL);
    exit(1);
}

$hibpClient = new HaveIBeenPwnedApiClient(new Client(['base_uri' => HaveIBeenPwnedApiClient::BASE_URI]), $apiKey);
$ppClient   = new PwnedPasswordsApiClient(new Client(['base_uri' => PwnedPasswordsApiClient::BASE_URI]));
$cilician   = new Cilician($hibpClient, $ppClient, $cache);

$runner = new \Jgxvx\CilicianRunner\Runner($cilician, $cache);

$runner->run();
