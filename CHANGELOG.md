# jgxvx/cilician-runner

## 4.0.0 - 2023-12-25
* Adding support for PHP 8.3
* Requiring Cilician 4.0
* Adding support for Symfony 7
* Removing support for Symfony <6.4
