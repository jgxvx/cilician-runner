<?php

/*
 * This file is part of jgxvx/cilician-runner.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\CilicianRunner;

use Jgxvx\Cilician\Service\Cilician;
use Jgxvx\CilicianRunner\Command\BreachesCommand;
use Jgxvx\CilicianRunner\Command\CheckPasswordCommand;
use Jgxvx\CilicianRunner\Command\ClearCacheCommand;
use Jgxvx\CilicianRunner\Command\DataClassesCommand;
use Jgxvx\CilicianRunner\Command\PastesCommand;
use Jgxvx\CilicianRunner\Command\SiteCommand;
use Jgxvx\CilicianRunner\Command\SitesCommand;
use Psr\SimpleCache\CacheInterface;
use Symfony\Component\Console\Application;

final class Runner extends Application
{
    public const NAME = 'Cilician Runner';

    private const VERSION = '0.1.0';

    public function __construct(Cilician $cilician, CacheInterface $cache)
    {
        parent::__construct(self::NAME, self::VERSION);

        $this->add(new CheckPasswordCommand($cilician));
        $this->add(new BreachesCommand($cilician));
        $this->add(new PastesCommand($cilician));
        $this->add(new SitesCommand($cilician));
        $this->add(new SiteCommand($cilician));
        $this->add(new DataClassesCommand($cilician));
        $this->add(new ClearCacheCommand($cache));
    }
}
