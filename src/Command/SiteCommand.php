<?php
/*
 * This file is part of jgxvx/cilician-runner.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\CilicianRunner\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SiteCommand extends BaseCommand
{
    private const COMMAND_NAME = 'site';

    protected function configure(): void
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDefinition([
                new InputArgument('name', InputArgument::REQUIRED, 'The name to filter by'),
            ])
            ->setDescription('Get a single breached site');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io   = new SymfonyStyle($input, $output);
        $name = $input->getArgument('name');

        try {
            $result = $this->cilician->getBreachedSite($name);
        } catch (\Exception $ex) {
            $io->error($ex->getMessage());

            return 1;
        }

        $breaches      = $result->getBreaches();
        $breachesCount = \count($breaches);

        if ($output->isVerbose()) {
            $io->table(
                ['Compromised', 'Number of Breaches', 'Cached Result'],
                [
                    [$this->yesOrNo($result->isPwned()), $breachesCount, $this->yesOrNo($result->isCached())],
                ],
            );

            foreach ($breaches as $breach) {
                $this->plotBreachModel($breach, $io);
            }
        }

        if ($result->isPwned()) {
            $io->warning("Site {$name} has been breached!");

            return 1;
        }

        $io->success("There are no known breaches of site {$name}");

        return 0;
    }
}
