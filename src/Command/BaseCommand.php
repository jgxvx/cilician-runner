<?php

/*
 * This file is part of jgxvx/cilician-runner.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\CilicianRunner\Command;

use Jgxvx\Cilician\Filter\BreachFilter;
use Jgxvx\Cilician\Model\BreachModel;
use Jgxvx\Cilician\Model\PasteModel;
use Jgxvx\Cilician\Service\Cilician;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;

abstract class BaseCommand extends Command
{
    protected const DATE_FORMAT = 'Y-m-d';

    /** @var Cilician */
    protected $cilician;

    public function __construct(Cilician $cilician)
    {
        parent::__construct();

        $this->cilician = $cilician;
    }

    protected function yesOrNo(bool $condition): string
    {
        return $condition ? 'Yes' : 'No';
    }

    protected function plotBreachFilter(BreachFilter $filter, SymfonyStyle $io): void
    {
        $io->table(
            ['Domain', 'Include Unverified'],
            [
                [$filter->getDomain() ?? '-', $this->yesOrNo($filter->getIncludeUnverified())],
            ],
        );
    }

    protected function plotBreachModel(BreachModel $breach, SymfonyStyle $io): void
    {
        $io->section('Breach: ' . $breach->name);
        $io->table(
            [],
            [
                ['Title', $breach->title],
                ['Domain', $breach->domain],
                ['Breach Date', $breach->breachDate?->format(static::DATE_FORMAT)],
                ['Added Date', $breach->addedDate?->format(static::DATE_FORMAT)],
                ['Modified Date', $breach->modifiedDate?->format(static::DATE_FORMAT)],
                ['Accounts Affected', \number_format($breach->pwnCount)],
                ['Data Classes', \implode(', ', $breach->dataClasses)],
                ['Verified', $this->yesOrNo($breach->isVerified)],
                ['Fabricated', $this->yesOrNo($breach->isFabricated)],
                ['Sensitive', $this->yesOrNo($breach->isSensitive)],
                ['Retired', $this->yesOrNo($breach->isRetired)],
                ['Spam List', $this->yesOrNo($breach->isSpamList)],
            ],
        );
    }

    protected function plotPasteModel(PasteModel $paste, SymfonyStyle $io): void
    {
        $io->section('Paste: ' . ($paste->title ?: '-'));

        $date = $paste->date ? $paste->date->format(static::DATE_FORMAT) : '-';

        $io->table(
            [],
            [
                ['ID', $paste->id],
                ['Date', $date],
                ['E-Mails Affected', $paste->emailCount],
                ['Source', $paste->source],
            ],
        );
    }
}
