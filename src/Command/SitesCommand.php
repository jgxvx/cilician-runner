<?php

/*
 * This file is part of jgxvx/cilician-runner.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\CilicianRunner\Command;

use Jgxvx\Cilician\Filter\BreachFilter;
use Jgxvx\CilicianRunner\Runner;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SitesCommand extends BaseCommand
{
    private const COMMAND_NAME = 'sites';

    protected function configure(): void
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDefinition(
                [
                    new InputOption('domain', null, InputOption::VALUE_REQUIRED, 'Domain name to filter by'),
                    new InputOption('include-unverified', null, InputOption::VALUE_NONE, 'Whether to include unverified breaches'),
                ],
            )
            ->setDescription('Get all breached sites in the system');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io                = new SymfonyStyle($input, $output);
        $domain            = $input->getOption('domain');
        $includeUnverified = (bool) $input->getOption('include-unverified');
        $filter            = ($domain || $includeUnverified) ? new BreachFilter() : null;

        $io->title(Runner::NAME . ': Get Breached Sites');

        if ($filter) {
            if ($domain) {
                $filter->setDomain($domain);
            }

            $filter->setIncludeUnverified($includeUnverified);

            $io->note('A filter is applied!');

            if ($output->isVeryVerbose()) {
                $this->plotBreachFilter($filter, $io);
            }
        }

        try {
            $result = $this->cilician->getBreachedSites($filter);
        } catch (\Exception $ex) {
            $io->error($ex->getMessage());

            return 1;
        }

        $breaches      = $result->getBreaches();
        $breachesCount = \count($breaches);

        if ($output->isVeryVerbose()) {
            $io->section('Result');
        }

        if ($output->isVerbose()) {
            $io->table(
                ['Compromised', 'Number of Breaches', 'Cached Result'],
                [
                    [$this->yesOrNo($result->isPwned()), $breachesCount, $this->yesOrNo($result->isCached())],
                ],
            );

            foreach ($breaches as $breach) {
                $this->plotBreachModel($breach, $io);
            }
        }

        if ($result->isPwned()) {
            $sites = ($breachesCount === 1) ? 'site' : 'sites';

            $io->warning("{$breachesCount} breached {$sites} found.");

            return 1;
        }

        $io->success('No breached sites found!');

        return 0;
    }
}
