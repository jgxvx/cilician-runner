<?php

/*
 * This file is part of jgxvx/cilician-runner.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\CilicianRunner\Command;

use Jgxvx\CilicianRunner\Runner;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PastesCommand extends BaseCommand
{
    private const COMMAND_NAME = 'pastes';

    protected function configure(): void
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDefinition(
                [
                    new InputArgument('account', InputArgument::REQUIRED, 'The account to search pastes for'),
                ],
            )
            ->setDescription('Get all pastes for an account');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io      = new SymfonyStyle($input, $output);
        $account = $input->getArgument('account');

        $io->title(Runner::NAME . ': Get Pastes for Account');

        try {
            $result = $this->cilician->getPastesForAccount($account);
        } catch (\Exception $ex) {
            $io->error($ex->getMessage());

            return 1;
        }

        $pastes      = $result->getPastes();
        $pastesCount = \count($pastes);

        if ($output->isVerbose()) {
            $io->table(
                ['Compromised', 'Number of Pastes', 'Cached Result'],
                [
                    [$this->yesOrNo($result->isPwned()), $pastesCount, $this->yesOrNo($result->isCached())],
                ],
            );

            foreach ($pastes as $paste) {
                $this->plotPasteModel($paste, $io);
            }
        }

        if ($result->isPwned()) {
            $io->warning("The account {$account} has been compromised!");

            return 1;
        }

        $io->success("There are no known pastes containing account {$account}");

        return 0;
    }
}
