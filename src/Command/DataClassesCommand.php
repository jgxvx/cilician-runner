<?php
/*
 * This file is part of jgxvx/cilician-runner.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\CilicianRunner\Command;

use Jgxvx\CilicianRunner\Runner;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DataClassesCommand extends BaseCommand
{
    private const COMMAND_NAME = 'data-classes';

    protected function configure(): void
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDefinition(
                [],
            )
            ->setDescription('Get all data classes in the system');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->title(Runner::NAME . ': Get Data Classes');

        try {
            $result = $this->cilician->getDataClasses();
        } catch (\Exception $ex) {
            $io->error($ex->getMessage());

            return 1;
        }

        $io->listing($result->getDataClasses());

        return 0;
    }
}
