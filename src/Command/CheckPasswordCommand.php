<?php

/*
 * This file is part of jgxvx/cilician-runner.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\CilicianRunner\Command;

use Jgxvx\CilicianRunner\Runner;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CheckPasswordCommand extends BaseCommand
{
    private const COMMAND_NAME = 'password';

    protected function configure(): void
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDefinition([
                new InputArgument('password', InputArgument::REQUIRED, 'The plaintext password to check'),
            ])
            ->setDescription('Checks if a password has appeared in a data breach');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $password = $input->getArgument('password');
        $io       = new SymfonyStyle($input, $output);

        $io->title(Runner::NAME . ': Password Check');

        $result = $this->cilician->checkPassword($password);

        if ($output->isVerbose()) {
            $io->table(
                ['Compromised', 'Count', 'Cached Result'],
                [
                    [$this->yesOrNo($result->isPwned()), $result->getCount(), $this->yesOrNo($result->isCached())],
                ],
            );
        }

        if ($result->isPwned()) {
            $io->warning('The provided password is not safe to use.');

            return 1;
        }

        $io->success('The provided password is probably safe to use.');

        return 0;
    }
}
