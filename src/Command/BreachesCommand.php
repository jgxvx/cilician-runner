<?php

/*
 * This file is part of jgxvx/cilician-runner.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\CilicianRunner\Command;

use Jgxvx\Cilician\Filter\BreachFilter;
use Jgxvx\CilicianRunner\Runner;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class BreachesCommand extends BaseCommand
{
    private const COMMAND_NAME = 'breaches';

    protected function configure(): void
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDefinition(
                [
                    new InputArgument('account', InputArgument::REQUIRED, 'The account to search breaches for'),
                    new InputOption('domain', 'd', InputOption::VALUE_REQUIRED, 'A domain name to filter for'),
                    new InputOption('include-unverified', null, InputOption::VALUE_NONE, 'Include unverified breaches'),
                ],
            )
            ->setDescription('Get all breaches for an account');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io                = new SymfonyStyle($input, $output);
        $account           = (string) $input->getArgument('account');
        $domain            = (string) $input->getOption('domain');
        $includeUnverified = (bool) $input->getOption('include-unverified');
        $filter            = ($domain || $includeUnverified) ? new BreachFilter() : null;

        $io->title(Runner::NAME . ': Get Breaches for Account');

        if ($filter) {
            if ($domain) {
                $filter->setDomain($domain);
            }

            $filter->setIncludeUnverified($includeUnverified);

            $io->note('A filter is applied!');

            if ($output->isVeryVerbose()) {
                $this->plotBreachFilter($filter, $io);
            }
        }

        try {
            $result = $this->cilician->getBreachesForAccount($account, $filter);
        } catch (\Exception $ex) {
            $io->error($ex->getMessage());

            return 1;
        }

        $breaches      = $result->getBreaches();
        $breachesCount = \count($breaches);

        if ($output->isVeryVerbose()) {
            $io->section('Result');
        }

        if ($output->isVerbose()) {
            $io->table(
                ['Compromised', 'Number of Breaches', 'Cached Result'],
                [
                    [$this->yesOrNo($result->isPwned()), $breachesCount, $this->yesOrNo($result->isCached())],
                ],
            );

            foreach ($breaches as $breach) {
                $this->plotBreachModel($breach, $io);
            }
        }

        if ($result->isPwned()) {
            $io->warning("The account {$account} has been compromised!");

            return 1;
        }

        $io->success("There are no known breaches containing account {$account}");

        return 0;
    }
}
