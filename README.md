# Cilician Runner

Command Line Interface for the [Cilician][1] library, a PHP client to the [haveibeenpwned.com][2] APIs.

[![pipeline status](https://gitlab.com/jgxvx/cilician-runner/badges/main/pipeline.svg)](https://gitlab.com/jgxvx/cilician-runner/commits/main)
[![coverage report](https://gitlab.com/jgxvx/cilician-runner/badges/main/coverage.svg?job=test)](https://gitlab.com/jgxvx/cilician-runner/commits/main)

## Prerequisites

The environment variable `HIBP_API_KEY` must be set with the value of a valid [Have I Been Pwned API key][10].

## Installation

The recommended way to install Cilician is by using [Composer][3]:

```bash
$ composer global require "jgxvx/cilician-runner"
```

The installation can be verified by invoking

```bash
$ cilician --version
Cilician Runner 0.1.0
```
If the cilician executable is not found, make sure [the global Composer directory is in the PATH][4], or symlink the executable to a location in your PATH, e.g.

```bash
$ ln -s ~/.composer/vendor/bin/cilician /usr/local/bin/cilician
```

## Usage

### Commands

To see a list of available commands, run

```bash
$ cilician list
```
To get help for a specific command, run

```bash
$ cilician help <command>
```

### Verbosity

The `-v` option can be passed to all commands for more verbose output.

### Check a Password

To see if a password has appeared in one or more breaches, run

```bash
$ cilician password 12345678 -v
```

### Getting All Breaches for an Account

To see if the account john@doe.com has been breached, simply run

```bash
$ cilician breaches john@doe.com -v
```

To narrow down the search to a specific domain:

```bash
$ cilician breaches john@doe.com --domain=example.com -v
```

By default, only verified breaches are returned. To include unverified breaches, pass the following option to the command:

```bash
$ cilician breaches john@doe.com --include-unverified -v
```

### Getting All Breached Sites in the System

To see a list of breached sites, run

```bash
$ cilician sites -v
```

This too, can be filtered by domain:

```bash
$ cilician sites --domain=example.com -v
```

### Getting a Single Breached Site

To get information on a specific site, run

```bash
$ cilician site Adobe -v
```

### Getting All Data Classes in the System

To get a list of all available [data classes][5] in the system, run

```bash
$ cilician data-classes
```

### Getting All Pastes for an Account

Similar to breaches, a search can be made for all [pastes][6] of an account:

```bash
$ cilician pastes john@doe.com -v
```

### Clearing the Cache

Cilician Runner uses a filesystem-based cache to optimise the requests made to the API. The cache directory is located at `./.cache` in the root directory of your Cilician installation.

To clear the cache, run

```bash
$ cilician clear-cache
```

## License

Cilician is open source software published under the MIT license. Please refer to the [LICENSE][7] file for further information.


## Contributing

We appreciate your help! To contribute, please read the [Contribution Guidelines][8] and our [Code of Conduct][9].

[1]: https://gitlab.com/jgxvx/cilician
[2]: https://haveibeenpwned.com
[3]: https://getcomposer.org
[4]: https://getcomposer.org/doc/03-cli.md#global
[5]: https://haveibeenpwned.com/API/v2#AllDataClasses
[6]: https://haveibeenpwned.com/API/v2#PastesForAccount
[7]: https://gitlab.com/jgxvx/cilician-runner/blob/master/LICENSE
[8]: https://gitlab.com/jgxvx/cilician-runner/blob/master/CONTRIBUTING.md
[9]: https://gitlab.com/jgxvx/cilician-runner/blob/master/CODE_OF_CONDUCT.md
[10]: https://haveibeenpwned.com/API/Key
