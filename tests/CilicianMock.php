<?php

/*
 * This file is part of jgxvx/cilician-runner.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\CilicianRunner;

use Jgxvx\Cilician\Exception\RateLimitException;
use Jgxvx\Cilician\Filter\BreachFilter;
use Jgxvx\Cilician\Model\BreachModel;
use Jgxvx\Cilician\Model\PasteModel;
use Jgxvx\Cilician\Result\BreachResult;
use Jgxvx\Cilician\Result\PasswordCheckResult;
use Jgxvx\Cilician\Result\PasteResult;
use Jgxvx\Cilician\Service\Cilician;

class CilicianMock
{
    public const SAFE_PASSWORD = '466cB0-b5c8f5Z9eb0-0f8bc289321-82c3';

    public const UNSAFE_PASSWORD = '12345678';

    /**
     * @return Cilician|\Mockery\MockInterface
     */
    public static function get()
    {
        $mock = \Mockery::mock(Cilician::class);

        $mock->shouldReceive('getBreachesForAccount')->andReturnUsing(function (string $account) {
            if ($account === 'john@doe.com') {
                $breach1 = BreachModel::fromArray(
                    [
                        'name'         => 'Adobe',
                        'title'        => 'Adobe',
                        'domain'       => 'adobe.com',
                        'breachDate'   => '2018-10-02',
                        'addedDate'    => '2018-10-02',
                        'modifiedDate' => '2018-10-02',
                        'pwnCount'     => '123',
                        'verified'     => true,
                        'fabricated'   => false,
                        'sensitive'    => false,
                        'retired'      => false,
                        'spamList'     => false,
                    ],
                );
                $result = new BreachResult([$breach1]);
            } elseif ($account === 'jack@doe.com') {
                throw new RateLimitException(2);
            } else {
                $result = new BreachResult();
            }

            return $result;
        });

        $mock->shouldReceive('checkPassword')->andReturnUsing(function (string $password) {
            if ($password === static::UNSAFE_PASSWORD) {
                $count = 123456789;
            } else {
                $count = 0;
            }

            return new PasswordCheckResult($password, $count);
        });

        $mock->shouldReceive('getPastesForAccount')->andReturnUsing(function (string $account) {
            if ($account === 'john@doe.com') {
                $paste1 = PasteModel::fromArray([
                    'Source'     => 'Pastie',
                    'Title'      => 'syslog',
                    'Id'         => '7152479',
                    'Date'       => '2013-03-28T16:51:10Z',
                    'EmailCount' => 30,
                ]);

                $result = new PasteResult([$paste1]);
            } elseif ($account === 'jack@doe.com') {
                throw new RateLimitException(2);
            } else {
                $result = new PasteResult([]);
            }

            return $result;
        });

        $mock->shouldReceive('getBreachedSite')->andReturnUsing(function (string $name) {
            if ($name === 'Adobe') {
                $breach = BreachModel::fromArray(
                    [
                        'name'         => 'Adobe',
                        'title'        => 'Adobe',
                        'domain'       => 'adobe.com',
                        'breachDate'   => '2018-10-02',
                        'addedDate'    => '2018-10-02',
                        'modifiedDate' => '2018-10-02',
                        'pwnCount'     => '123',
                        'verified'     => true,
                        'fabricated'   => false,
                        'sensitive'    => false,
                        'retired'      => false,
                        'spamList'     => false,
                    ],
                );

                $result = new BreachResult([$breach]);
            } elseif ($name === 'RateLimiter') {
                throw new RateLimitException(2);
            } else {
                $result = new BreachResult([]);
            }

            return $result;
        });

        $mock->shouldReceive('getBreachedSites')->andReturnUsing(function (BreachFilter $filter = null) {
            $adobe    = static::getAdobeBreach();
            $linkedIn = static::getLinkedInBreach();

            if ($filter === null) {
                $result = new BreachResult([$adobe, $linkedIn]);
            } elseif ($filter->getDomain() === 'adobe.com') {
                $result = new BreachResult([$adobe]);
            } elseif ($filter->getDomain() === 'ratelimiter.com') {
                throw new RateLimitException(2);
            } else {
                $result = new BreachResult();
            }

            return $result;
        });

        return $mock;
    }

    private static function getAdobeBreach(): BreachModel
    {
        return BreachModel::fromArray(
            [
                'name'         => 'Adobe',
                'title'        => 'Adobe',
                'domain'       => 'adobe.com',
                'breachDate'   => '2018-10-02',
                'addedDate'    => '2018-10-02',
                'modifiedDate' => '2018-10-02',
                'pwnCount'     => '123',
                'verified'     => true,
                'fabricated'   => false,
                'sensitive'    => false,
                'retired'      => false,
                'spamList'     => false,
            ],
        );
    }

    private static function getLinkedInBreach(): BreachModel
    {
        return BreachModel::fromArray(
            [
                'name'         => 'LinkedIn',
                'title'        => 'LinkedIn',
                'domain'       => 'linkedin.com',
                'breachDate'   => '2018-10-02',
                'addedDate'    => '2018-10-02',
                'modifiedDate' => '2018-10-02',
                'pwnCount'     => '123',
                'verified'     => true,
                'fabricated'   => false,
                'sensitive'    => false,
                'retired'      => false,
                'spamList'     => false,
            ],
        );
    }
}
