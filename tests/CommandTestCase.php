<?php

/*
 * This file is part of jgxvx/cilician-runner.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\CilicianRunner;

use Jgxvx\Cilician\Service\Cilician;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @coversNothing
 */
class CommandTestCase extends TestCase
{
    protected function getCilician(): Cilician
    {
        return CilicianMock::get();
    }

    protected function executeCommand(Command $command, array $arguments): CommandTester
    {
        $application = new Application();
        $application->add($command);

        $command       = $application->find($command->getName());
        $commandTester = new CommandTester($command);

        $commandTester->execute(\array_merge(
            ['command' => $command->getName()],
            $arguments,
        ), [
            'verbosity' => OutputInterface::VERBOSITY_VERY_VERBOSE,
        ]);

        return $commandTester;
    }
}
