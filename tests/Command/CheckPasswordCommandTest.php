<?php

/*
 * This file is part of jgxvx/cilician-runner.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\CilicianRunner\Command;

use Jgxvx\CilicianRunner\CilicianMock;
use Jgxvx\CilicianRunner\CommandTestCase;

/**
 * @covers \Jgxvx\CilicianRunner\Command\CheckPasswordCommand
 */
class CheckPasswordCommandTest extends CommandTestCase
{
    public function testIfCommandFailsWithoutPassword(): void
    {
        $this->expectExceptionMessage('Not enough arguments (missing: "password").');
        $this->expectException(\Symfony\Component\Console\Exception\RuntimeException::class);
        $this->executeCommand($this->createCommand(), []);
    }

    public function testIfPasswordIsFound(): void
    {
        $cmd = $this->executeCommand($this->createCommand(), ['password' => CilicianMock::UNSAFE_PASSWORD]);

        self::assertSame(1, $cmd->getStatusCode());
        self::assertStringContainsString('The provided password is not safe to use.', $cmd->getDisplay(true));
    }

    public function testIfPasswordIsNotFound(): void
    {
        $cmd = $this->executeCommand($this->createCommand(), ['password' => CilicianMock::SAFE_PASSWORD]);

        self::assertSame(0, $cmd->getStatusCode());
        self::assertStringContainsString('The provided password is probably safe to use.', $cmd->getDisplay(true));
    }

    private function createCommand(): CheckPasswordCommand
    {
        return new CheckPasswordCommand($this->getCilician());
    }
}
