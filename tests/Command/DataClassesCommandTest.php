<?php
/*
 * This file is part of jgxvx/cilician-runner.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\CilicianRunner\Command;

use Jgxvx\Cilician\Exception\RateLimitException;
use Jgxvx\Cilician\Result\DataClassResult;
use Jgxvx\Cilician\Service\Cilician;
use Jgxvx\CilicianRunner\CommandTestCase;

/**
 * @covers \Jgxvx\CilicianRunner\Command\DataClassesCommand
 */
class DataClassesCommandTest extends CommandTestCase
{
    public function testIfDataClassesAreLoaded(): void
    {
        $command = $this->createCommand();

        $cmd = $this->executeCommand($command, []);
        self::assertSame(0, $cmd->getStatusCode());
        self::assertStringContainsString('* foo', $cmd->getDisplay(true));
        self::assertStringContainsString('* bar', $cmd->getDisplay(true));

        $cmd = $this->executeCommand($command, []);
        self::assertSame(1, $cmd->getStatusCode());
        self::assertStringContainsString('Rate limit exceeded. Wait for 2 second(s).', $cmd->getDisplay(true));
    }

    private function createCommand(): DataClassesCommand
    {
        $cilician = \Mockery::mock(Cilician::class);

        $cilician->shouldReceive('getDataClasses')->once()->andReturn(new DataClassResult(['foo', 'bar']));
        $cilician->shouldReceive('getDataClasses')->once()->andThrow(new RateLimitException(2));

        return new DataClassesCommand($cilician);
    }
}
