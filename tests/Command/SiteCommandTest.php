<?php
/*
 * This file is part of jgxvx/cilician-runner.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\CilicianRunner\Command;

use Jgxvx\CilicianRunner\CommandTestCase;

/**
 * @covers \Jgxvx\CilicianRunner\Command\SiteCommand
 */
class SiteCommandTest extends CommandTestCase
{
    public function testIfCommandFailsWithoutAccount(): void
    {
        $this->expectExceptionMessage('Not enough arguments (missing: "name").');
        $this->expectException(\Symfony\Component\Console\Exception\RuntimeException::class);
        $this->executeCommand($this->createCommand(), []);
    }

    public function testIfSiteIsFound(): void
    {
        $cmd = $this->executeCommand($this->createCommand(), ['name' => 'Adobe']);

        self::assertSame(1, $cmd->getStatusCode());
        self::assertStringContainsString('Site Adobe has been breached!', $cmd->getDisplay(true));
    }

    public function testIfSiteIsNotFound(): void
    {
        $cmd = $this->executeCommand($this->createCommand(), ['name' => 'SafeSite']);

        self::assertSame(0, $cmd->getStatusCode());
        self::assertStringContainsString('There are no known breaches of site SafeSite', $cmd->getDisplay(true));
    }

    public function testIfExceptionIsHandled(): void
    {
        $cmd = $this->executeCommand($this->createCommand(), ['name' => 'RateLimiter']);

        self::assertSame(1, $cmd->getStatusCode());
        self::assertStringContainsString('Rate limit exceeded. Wait for 2 second(s).', $cmd->getDisplay(true));
    }

    private function createCommand(): SiteCommand
    {
        return new SiteCommand($this->getCilician());
    }
}
