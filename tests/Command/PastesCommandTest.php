<?php
/*
 * This file is part of jgxvx/cilician-runner.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\CilicianRunner\Command;

use Jgxvx\CilicianRunner\CommandTestCase;

/**
 * @covers \Jgxvx\CilicianRunner\Command\PastesCommand
 */
class PastesCommandTest extends CommandTestCase
{
    public function testIfCommandFailsWithoutAccount(): void
    {
        $this->expectExceptionMessage('Not enough arguments (missing: "account").');
        $this->expectException(\Symfony\Component\Console\Exception\RuntimeException::class);
        $this->executeCommand($this->createCommand(), []);
    }

    public function testIfPastesAreFoundForAccount(): void
    {
        $cmd = $this->executeCommand($this->createCommand(), ['account' => 'john@doe.com']);

        self::assertSame(1, $cmd->getStatusCode());
        self::assertStringContainsString('The account john@doe.com has been compromised!', $cmd->getDisplay(true));
    }

    public function testIfNoPastesAreFoundForAccount(): void
    {
        $cmd = $this->executeCommand($this->createCommand(), ['account' => 'jane@doe.com']);

        self::assertSame(0, $cmd->getStatusCode());
        self::assertStringContainsString('There are no known pastes containing account jane@doe.com', $cmd->getDisplay(true));
    }

    public function testIfExceptionIsHandled(): void
    {
        $cmd = $this->executeCommand($this->createCommand(), ['account' => 'jack@doe.com']);

        self::assertSame(1, $cmd->getStatusCode());
        self::assertStringContainsString('Rate limit exceeded. Wait for 2 second(s).', $cmd->getDisplay(true));
    }

    private function createCommand(): PastesCommand
    {
        return new PastesCommand($this->getCilician());
    }
}
