<?php
/*
 * This file is part of jgxvx/cilician-runner.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\CilicianRunner\Command;

use Jgxvx\CilicianRunner\CommandTestCase;
use Psr\SimpleCache\CacheInterface;

/**
 * @covers \Jgxvx\CilicianRunner\Command\ClearCacheCommand
 */
class ClearCacheCommandTest extends CommandTestCase
{
    public function testIfCacheIsCleared(): void
    {
        $command = $this->createCommand();

        $cmd = $this->executeCommand($command, []);
        self::assertSame(0, $cmd->getStatusCode());
        self::assertStringContainsString('Cache cleared!', $cmd->getDisplay(true));

        $cmd = $this->executeCommand($command, []);
        self::assertSame(1, $cmd->getStatusCode());
        self::assertStringContainsString('Cache could not be cleared.', $cmd->getDisplay(true));
    }

    private function createCommand(): ClearCacheCommand
    {
        $cache = \Mockery::mock(CacheInterface::class);

        $cache->shouldReceive('clear')->once()->andReturnTrue();
        $cache->shouldReceive('clear')->once()->andReturnFalse();

        return new ClearCacheCommand($cache);
    }
}
