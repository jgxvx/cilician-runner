<?php
/*
 * This file is part of jgxvx/cilician-runner.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\CilicianRunner\Command;

use Jgxvx\CilicianRunner\CommandTestCase;

/**
 * @covers \Jgxvx\CilicianRunner\Command\SitesCommand
 */
class SitesCommandTest extends CommandTestCase
{
    public function testIfSitesAreFoundWithoutFilter(): void
    {
        $cmd = $this->executeCommand($this->createCommand(), []);

        self::assertSame(1, $cmd->getStatusCode());
        self::assertStringContainsString('2 breached sites found.', $cmd->getDisplay(true));
    }

    public function testIfSiteIsFoundWithFilter(): void
    {
        $cmd = $this->executeCommand($this->createCommand(), ['--domain' => 'adobe.com']);

        self::assertSame(1, $cmd->getStatusCode());
        self::assertStringContainsString('1 breached site found.', $cmd->getDisplay(true));
    }

    public function testIfSafeSiteIsNotFound(): void
    {
        $cmd = $this->executeCommand($this->createCommand(), ['--domain' => 'safesite.com']);

        self::assertSame(0, $cmd->getStatusCode());
        self::assertStringContainsString('No breached sites found!', $cmd->getDisplay(true));
    }

    public function testIfExceptionIsHandled(): void
    {
        $cmd = $this->executeCommand($this->createCommand(), ['--domain' => 'ratelimiter.com']);

        self::assertSame(1, $cmd->getStatusCode());
        self::assertStringContainsString('Rate limit exceeded. Wait for 2 second(s).', $cmd->getDisplay(true));
    }

    private function createCommand(): SitesCommand
    {
        return new SitesCommand($this->getCilician());
    }
}
